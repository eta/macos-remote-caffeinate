import * as vscode from 'vscode';
import * as process from 'node:process';
import * as child_process from 'node:child_process';

export function activate(context: vscode.ExtensionContext) {
	let pid = process.pid;

	console.log(`o/ going to caffeinate waiting on this pid: ${pid}`);
	child_process.spawn('caffeinate', ['-w', pid.toString()]);
}

export function deactivate() {
}
