# macos-remote-caffeinate

Prevent your Mac from sleeping when using VS Code remotely.

This incredibly simple, basically one-line extension does one thing and one thing only: runs
[`caffeinate`](https://ss64.com/osx/caffeinate.html) with the Process ID of (what we hope is)
the VS Code extension host process. This prevents a Mac from sleeping until the extension host
process exits.

Why would you want this? Well, if you're running something like [`code-server`](https://github.com/coder/code-server)
to access VS Code on your Mac remotely, you might find the Mac goes to sleep while you're connected
in. This is because there's nothing to tell it to stay awake (the only reason this doesn't happen
when you SSH in remotely, for example, is because the OS is checking for remote `tty`s).

This incredibly dumb extension fixes this problem. (At least for me.)

## Installing

```
$ npx @vscode/vsce package
```

Then, install `/macos-remote-caffeinate-0.0.1.vsix` via **Extensions** > *triple-dot menu* > **Install from VSIX...**.

## LICENSE

MIT (there's really nothing here)